const http = require('http');
const app = require('./App');
const dotenv = require('dotenv');
dotenv.config();

const port = process.env.PORT || 3001;

const server = http.createServer(app);

server.listen(port, () => {
    console.log(`server is listening at port:  ${port}`);
});

