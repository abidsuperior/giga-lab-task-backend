const { validate, ValidationError, Joi } = require('express-validation');



module.exports.signupValidation = {
    body: Joi.object({
        name: Joi.string()
        //.regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        email: Joi.string()
        //.regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        phone: Joi.string()
       // .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        password: Joi.string() 
        // .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        profile: Joi.string() 
        // .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
    }),
  };

  


module.exports.createMoviesValidation = {
    body: Joi.object({
        title: Joi.string()
        //.regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        image_url: Joi.string()
        //.regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        rating: Joi.string()
       // .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        u_id: Joi.number()
        // .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        
    }),
  };

  
  module.exports.updateMoviesValidation = {
    body: Joi.object({
        id: Joi.string()
        //.regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        title: Joi.string()
        //.regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        image_url: Joi.string().allow(null).allow(""),
        //.regex(/[a-zA-Z0-9]{3,30}/)
        //.required(),
        rating: Joi.string()
       // .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        u_id: Joi.string() 
        // .regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        
    }),
  };

  
  module.exports.deleteMoviesValidation = {
    body: Joi.object({
        id: Joi.string()
        //.regex(/[a-zA-Z0-9]{3,30}/)
        .required(),
        
    }),
  };
  


