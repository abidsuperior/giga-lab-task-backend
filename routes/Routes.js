
const express = require('express');
const router = express.Router();


//Movies route
router.use('/movies',require('../Movies'));
router.use('/signup',require('../Signup'));
router.use('/auth',require('../Auth'));




module.exports = router;