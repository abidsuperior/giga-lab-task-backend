const express = require("express");
const route = express.Router();
const jwt = require("jsonwebtoken");
const fs = require("fs");
const path = require("path");
var appDir = path.dirname(require.main.filename);
var privateKEY = fs.readFileSync(appDir + "/private.key", "utf8");

route.post("/login", async (req, res, next) => {
  try {
    //console.log(req.body);

    let email = req.body.email;
    let password = req.body.password;

    let conn = req.conn;

    let query = `SELECT u.user_id,u.user_email, u.user_password FROM user_tbl u
        WHERE u.user_email = ?
        AND u.user_password =?;`;

    console.log(query);

    await conn.query(query, [email, password], (err, result) => {
      if (err) {
        console.error(err);
        res.status(400).json({
          status: false,
          statusCode: "400",
          message: "Bad Request",
          payload: null,
        });
        conn.release();
      } else {
        if (result.length > 0) {
          console.log(result);

          const token = jwt.sign(
            { id: result.user_id, email: result.user_email },
            privateKEY,
            { expiresIn: "300 days", algorithm: "RS256" }
          );

          res.status(200).json({
            status: true,
            statusCode: "200",
            message: "success",
            token: token,
            payload: result,
          });
        } else {
          res.status(401).json({
            status: false,
            statusCode: "401",
            message: "Authorization Failed",
            payload: null,
          });
        }

        conn.release();
      }
    });
  } catch (err) {
    console.error(err);
    res.status(500).json({
      status: false,
      statusCode: "500",
      message: "internal failure",
      payload: null,
    });
  }
});
module.exports = route;
