const jwt = require('jsonwebtoken');
const fs = require('fs');
const path = require('path');


module.exports = (req, res, next) => {
    var appDir = path.dirname(require.main.filename);
    var publicKEY = fs.readFileSync(appDir + '/public.key', 'utf8');

    try {
        const token = req.headers.authorization;  
          
        console.log(token);

        const decode = jwt.verify(token, publicKEY, { expiresIn: "24h", algorithm: ['RS256'] });
        req.userData = decode;
        next();
    }
    catch (err) {
        console.log(err);
        return  res.status(401).json({
            status: false,
            statusCode: "401",
            message: "Authorization Failed",
            payload: null,
          });
       
    }
};