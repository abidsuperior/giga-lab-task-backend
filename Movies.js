const express = require("express");
const router = express.Router();
const auth = require('./middleware/clientAuth');
const { validate, ValidationError, Joi } = require('express-validation');
const validation = require('./Validations');

//To retrive list of movies
router.get("/listMovies",  async (req, res) => {
  try {
    //console.log(req.body);

    let conn = req.conn;

    let query = `SELECT * FROM movies_tbl;`;

    await conn.query(query, (err, result) => {
      if (err) {
        console.error(result);
        res.status(400).json({
          status: false,
          statusCode: "400",
          message: "Bad Request",
          payload: null,
        });

        conn.release();
      } else {
        res.status(200).json({
          status: true,
          statusCode: "200",
          message: "success",
          payload: result,
        });

        conn.release();
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: "500",
      message: "internal failure",
      payload: null,
    });
  }
});

//To create movie
router.post("/createMovie", validate(validation.createMoviesValidation, {},{}),auth, async (req, res) => {
  try {
    console.log(req.body);

    

    let title = req.body.title;
    let image_url = req.body.image_url;
    let rating = req.body.rating;
    let u_id = req.body.u_id;

    let conn = req.conn;

    let query = `INSERT INTO  movies_tbl (
            movie_title,
            movie_image_url,
            movie_rating,
            user_id            
            )VALUES(?,?,?,?);`;

    await conn.query(query, [title, image_url, rating,u_id], (err, result) => {
      if (err) {
        console.error(err);
        res.status(400).json({
          status: false,
          statusCode: "400",
          message: "Bad Request",
          payload: null,
        });

        conn.release();
      } else {
        res.status(200).json({
          status: true,
          statusCode: "200",
          message: "success",
          payload: result,
        });
        conn.release();
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: "500",
      message: "internal failure",
      payload: null,
    });
  }
});

//to update movie
router.put("/updateMovie", validate(validation.updateMoviesValidation, {},{}),auth ,async (req, res) => {
  try {
    console.log(req.body);

    let title = req.body.title;
    let image_url = req.body.image_url;
    let rating = req.body.rating;
    let u_id = req.body.u_id;
    let id = req.body.id;

    let conn = req.conn;

    let query = `UPDATE movies_tbl
            SET movie_title = ?,
            movie_image_url = ?,
            movie_rating = ?,
            user_id = ?
            WHERE movie_id = ?;`;

    await conn.query(query, [title, image_url, rating,u_id, id], (err, result) => {
      if (err) {
        console.error(err);
        res.status(400).json({
          status: false,
          statusCode: "400",
          message: "Bad Request",
          payload: null,
        });

        conn.release();
      } else {
        res.status(200).json({
          status: true,
          statusCode: "200",
          message: "success",
          payload: result,
        });
        conn.release();
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: "500",
      message: "internal failure",
      payload: null,
    });
  }
});

//to delete movie
router.delete("/deleteMovie", validate(validation.deleteMoviesValidation, {},{}), auth, async (req, res) => {
  // console.log(req.body);
  // console.log(req.headers.authorization);
  try {
    

    let id = req.body.id;

    let conn = req.conn;

    let query = `DELETE FROM movies_tbl
            WHERE movie_id = ?;`;

    await conn.query(query, [id], (err, result) => {
      if (err) {
        console.error(err);
        res.status(400).json({
          status: false,
          statusCode: "400",
          message: "Bad Request",
          payload: null,
        });

        conn.release();
      } else {
        res.status(200).json({
          status: true,
          statusCode: "200",
          message: "success",
          payload: result,
        });
        conn.release();
      }
    });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      status: false,
      statusCode: "500",
      message: "internal failure",
      payload: null,
    });
  }
});

module.exports = router;
