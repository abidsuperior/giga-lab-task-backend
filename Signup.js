const express = require("express");
const router = express.Router();
const auth = require('./middleware/clientAuth');
const { validate, ValidationError, Joi } = require('express-validation');
const validation = require('./Validations');

//To create user in the database
router.post("/createUser",  validate(validation.signupValidation, {},{}), auth, async (req, res) => {
  //console.log(req.body);
  try {
    //parse request body
    let name = req.body.name;
    let email = req.body.email;
    let phone = req.body.phone;
    let password = req.body.password;
    let profile = req.body.profile;

    let conn = req.conn;

    let query = `INSERT INTO  user_tbl (
        user_name,
        user_email,
        user_phone,
        user_password,
        user_profile_desc
        )VALUES(?,?,?,?,?);`;

    await conn.query(
      query,
      [name, email, phone, password, profile],
      (err, result) => {
        if (err) {
          console.log(err);

          console.error(result);
          res.status(400).json({
            status: false,
            statusCode: "400",
            message: "Bad Request",
            payload: null,
          });
          conn.release();
        } else {
          res.status(200).json({
            status: true,
            statusCode: "200",
            message: "success",
            payload: result,
          });
          conn.release();
        }
      }
    );
  } catch (err) {
    console.error(err);
    res.status(500).json({
      status: false,
      statusCode: "500",
      message: "internal failure",
      payload: null,
    });
  }
});

module.exports = router;
