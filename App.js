

const express = require('express');
const bodyParser = require('body-parser');
const helmet = require('helmet')

const xss = require('xss-clean');
const auth = require('./middleware/clientAuth');
const cors = require('cors')
const path = require('path');


const mysql = require('mysql');
const dbConfig = require('./dbConfig');
const { validate, ValidationError, Joi } = require('express-validation');
const { json } = require('express');

const app = express();


 app.use(express.json());
 app.use(express.urlencoded({ extended: true, limit: "50mb" }));

 //to prevent xss.
 app.use(xss());
 //Helmet is a collection of middleware functions.
 app.use(helmet());
 //to handle cors errors. 
 app.use(cors());


 const pool = mysql.createPool(dbConfig, {multipleStatements: true});

//sending connection to child routes. 
const poolFunc = async function (req, res, next) {

    try{
       pool.getConnection((err,conn) =>{
           if(err){
               console.log(err);
               
           }else{
               req.conn = conn;
               req.pool = pool;
               next();
           }

       });

    }
    catch(err){
      console.log(err);
      console.log("no connection");
    }

    

 };


app.use('/1.0',poolFunc,require('./routes/Routes'));


app.all('/', (req, res) => {
    return res.json({
        name: 'wild card URL'
    }); 	
    });

    


  app.use((error, req, res, next) => {
    if (error instanceof ValidationError) {
        return res.status(error.statusCode).json(error)
      }
    res.status(error.status || 500);
    res.json({
        message: error.message
       
    });
});

module.exports = app;